var planilha = 'https://docs.google.com/spreadsheets/d/17Gw3m11Q6LunJdQTk2SiknpGhi-oQ2eojIyVMhxWmpw/edit#gid=0';
var num, localidades, qntParticipantes;
var retornoSexo, retornoSexoQnt1, retornoSexoQnt2, retornoSexoQnt3;

$('#numParticipantes').sheetrock({
	url: planilha,
	query: "select count (A)",
	callback: retornoQntParticipantes
});
function retornoQntParticipantes(error, options, response){
	qntParticipantes = response.rows[1].cellsArray[0];
	oMapa();	
}
$('#localidades').sheetrock({
	url: planilha,
	query: "select count(X)",
	callback: retornoLocalidades
});
function retornoLocalidades(error, options, response){
	localidades = response.rows[1].cellsArray[0];
	localidades = Number(localidades);
	
	$('#dadosProveniencia').sheetrock({
	url: planilha,
	query: "select W, X where X > 0 order by X desc",
	fetchSize: (localidades - 1),
	headersOff: true,
	labels: ['Local de origem', 'Quantidade de alunos']
});
}
$('#mediaIdade').sheetrock({
	query: "select F",
	url: planilha,
});
num = Math.floor(Math.random() * 11) + 1;
$('#quantCurso').sheetrock({
	url: planilha,
	query: "select H where J = " + num,
});
$('#nomeCurso').sheetrock({
	url: planilha,
	query: "select G where J = " + num,
});
$('#dadosSexo').sheetrock({
	url: planilha,
	query: "select K, L, M ",
	fetchSize: 2,
	callback: retornoSexoFunc
});
function retornoSexoFunc(error, options, response) {
	retornoSexoQnt1 = (response.rows[1].cellsArray[0].replace(",", ".")).replace("%", "");
	retornoSexoQnt2 = (response.rows[1].cellsArray[1].replace(",", ".")).replace("%", "");
	retornoSexoQnt3 = (response.rows[1].cellsArray[2].replace(",", ".")).replace("%", "");
	retornoSexo = response.rows[1].labels;
	
	graphSex();
}
function graphSex(){
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: retornoSexo,
    datasets: [{
      backgroundColor: [
        "#2ecc71",
        "#3498db",
		"#95a5a6"
      ],
      data: [retornoSexoQnt1, retornoSexoQnt2, retornoSexoQnt3]
    }]
  }
});
}
$('#idadeMax').sheetrock({
	url: planilha,
	query: "select MAX(E)"
});
$('#idadeMin').sheetrock({
	url: planilha,
	query: "select MIN(E)",
	callback: retornoIdadeMin
});
function retornoIdadeMin(error, options, response) {
	var idadeMin17 = response.rows[1].cellsArray[0];	
	$('#qntIdadeMin').sheetrock({
		url: planilha,
		query: "select O where N = " + idadeMin17
	});
}
$('#dadosIdade').sheetrock({
	url: planilha,
	query: "select N, O, P",
	fetchSize: 25,
	callback: retornoIdadeFunc
});
var labelsIdade = [], dadosIdade = [];
function retornoIdadeFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		dadosIdade.push(response.rows[i].cellsArray[1])
		labelsIdade.push(Number(response.rows[i].cellsArray[0]))
	}
	graphIdade();
}
function graphIdade(){
	var ctx = document.getElementById("graphIdade").getContext('2d');
	var graphIdade = new Chart(ctx, {
		type: 'line',
		data: {
			labels: labelsIdade,			
			datasets: [{
				label: "Idades",
				fill: false,
				lineTension: 0.1,
				backgroundColor: "rgba(75,192,192,0.4)",
				borderColor: "rgba(75,192,192,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,192,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(75,192,192,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: dadosIdade,
				spanGaps: false,
			}]
		}
	});
}
$('#dadosCurso').sheetrock({
	url: planilha,
	sql: "select Q, R",
	fetchSize: 10,
	headersOff: true,
	labels: ['Curso','Quantidade de alunos'],
	callback: retornoCursoFunc
});
var labelsCurso = [], dadosCurso = [];
function retornoCursoFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsCurso.push(response.rows[i].cellsArray[0])
		dadosCurso.push(Number(response.rows[i].cellsArray[1]))
	}
	graphCurso();
}
function graphCurso(){
	var ctx = document.getElementById("graphCurso").getContext('2d');
	var graphCurso = new Chart(ctx, {
		type: 'pie',//'horizontalBar',
		data: {
			labels: labelsCurso,
			datasets: [{
				label: "Gráfico curso",
				backgroundColor: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosCurso
			}]
		}
	});
}
var arrayCoordenadas = [],
	result;
function oMapa() {
	sheetrock({
		url: planilha,
		query: "select D",
		callback: callbackMapa
	});	
	function callbackMapa(error, options, response) {
		for (i = 1; i <= qntParticipantes; i++) {
			arrayCoordenadas[i-1] = JSON.parse(response.rows[i].cellsArray[0]);
			arrayCoordenadas[i-1].lat = Number(arrayCoordenadas[i-1].lat);
			arrayCoordenadas[i-1].lng = Number(arrayCoordenadas[i-1].lng); 
		}
		initMap();
	}
}
function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 3,
		center: {lat: -18.024, lng: -50.887}
	});
	map.setOptions({ minZoom: 3, maxZoom: 10 });

	var labels = '';
	var markers = locations.map(function(location, i) {
		return new google.maps.Marker({
			position: location,
			label: labels[i % labels.length]
		});
	});

  var markerCluster = new MarkerClusterer(map, markers,
     {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}
var locations = arrayCoordenadas;
$('#dadosAnoIngresso').sheetrock({
	url: planilha,
	query: "select T, U, V",
	fetchSize: 10,
	headersOff: true,
	labels: ['Ano de ingresso','Quantidade de alunos', 'Percentual (%)'],
	callback: retornoAnoIngressoFunc
});
var labelsAnoIngresso = [], dadosAnoIngresso = [];
function retornoAnoIngressoFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsAnoIngresso.push(response.rows[i].cellsArray[0])
		dadosAnoIngresso.push(Number(response.rows[i].cellsArray[1]))
	}
	graphAnoIngresso();
}
function graphAnoIngresso(){	
	var ctx = document.getElementById("graphAnoIngresso").getContext('2d');
	var graphAnoIngresso = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labelsAnoIngresso,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosAnoIngresso
			}]
		}
	});
}
$('#dadosVinda').sheetrock({
	url: planilha,
	query: "select AD, AE",
	fetchSize: 2,
	headersOff: true,
	labels: ['Sim','Não, eu já morava em Ponta Grossa ou nas proximidades'],
	callback: retornoDadosVindaFunc
});
var labelsDadosVinda = [], dadosDadosVinda = [];
function retornoDadosVindaFunc(error, options, response) {
	for (var i = 0; i <= response.rows.length - 2; i++){
		labelsDadosVinda.push(response.rows[0].cellsArray[i])
		dadosDadosVinda.push(response.rows[1].cellsArray[i])
	}
	graphDadosVinda();
}
function graphDadosVinda(){	
	var ctx = document.getElementById("graphDadosVinda").getContext('2d');
	var graphDadosVinda = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: labelsDadosVinda,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#a6cee3','#1f78b4'],
				data: dadosDadosVinda
			}]
		}
	});
}
$('#comoViveAntes').sheetrock({
	url: planilha,
	query: "select AF, AG, AH",
	fetchSize: 5,
	headersOff: true,
	labels: ['Como e com quem vivia','Quantidade', 'Percentual (%)'],
	callback: retornoComoViveAntesFunc
});
var labelsComoViveAntes = [], dadosComoViveAntes = [], dadosComoViveAtualmente = [];
function retornoComoViveAntesFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsComoViveAntes.push(response.rows[i].cellsArray[0])
		dadosComoViveAntes.push(Number(response.rows[i].cellsArray[1]))
	}
	graphComoViveAntes();
}
function graphComoViveAntes(){	
	var ctx = document.getElementById("graphComoViveAntes").getContext('2d');
	var graphComoViveAntes = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: labelsComoViveAntes,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosComoViveAntes
			}]
		}
	});
}
$('#comoViveAtualmente').sheetrock({
	url: planilha,
	query: "select AF, AJ, AI",
	fetchSize: 5,
	headersOff: true,
	labels: ['Como e com quem vive','Quantidade', 'Percentual (%)'],
	callback: retornoComoViveAtualmenteFunc
});
function retornoComoViveAtualmenteFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		dadosComoViveAtualmente.push(Number(response.rows[i].cellsArray[1]))
	}
	graphComoViveAtualmente();
}
function graphComoViveAtualmente(){	
	var ctx = document.getElementById("graphComoViveAtualmente").getContext('2d');
	var graphComoViveAtualmente = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: labelsComoViveAntes,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosComoViveAtualmente
			}]
		}
	});
	graphComoViveAtualmente.update();
}
$('#fonteRendaAntes').sheetrock({
	url: planilha,
	query: "select AK, AL, AM",
	fetchSize: 4,
	headersOff: true,
	labels: ['Fonte de renda','Quantidade', 'Percentual (%)'],
	callback: retornofonteRendaAntesFunc
});
var labelsfonteRendaAntes = [], dadosfonteRendaAntes = [];
function retornofonteRendaAntesFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsfonteRendaAntes.push(response.rows[i].cellsArray[0])
		dadosfonteRendaAntes.push(Number(response.rows[i].cellsArray[1]))
	}
	graphFonteRendaAntes();
}
function graphFonteRendaAntes(){	
	var ctx = document.getElementById("graphFonteRendaAntes").getContext('2d');
	var graphFonteRendaAntes = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: labelsfonteRendaAntes,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosfonteRendaAntes
			}]
		}
	});
}
$('#fonteRendaAtual').sheetrock({
	url: planilha,
	query: "select AP, AO, AN",
	fetchSize: 5,
	headersOff: true,
	labels: ['Fonte de renda','Quantidade', 'Percentual (%)'],
	callback: retornofonteRendaAtualFunc
});
var labelsfonteRendaAtual = [], dadosfonteRendaAtual = [];
function retornofonteRendaAtualFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsfonteRendaAtual.push(response.rows[i].cellsArray[0])
		dadosfonteRendaAtual.push(Number(response.rows[i].cellsArray[1]))
	}
	graphFonteRendaAtual();
}
function graphFonteRendaAtual(){	
	var ctx = document.getElementById("graphFonteRendaAtual").getContext('2d');
	var graphFonteRendaAtual = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: labelsfonteRendaAtual,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosfonteRendaAtual
			}]
		}
	});
}
$('#atividadesSociaisAntes').sheetrock({
	url: planilha,
	query: "select AQ, AR, AS",
	fetchSize: 4,
	headersOff: true,
	labels: ['Atividades','Quantidade', 'Percentual (%)'],
	callback: retornoAtividadesSociaisAntesFunc
});
var labelsAtividadesSociaisAntes = [], 
	dadosAtividadesSociaisAntes = [], 
	labelsAtividadesSociaisAtual = [], 
	dadosAtividadesSociaisAtual = [];
function retornoAtividadesSociaisAntesFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsAtividadesSociaisAntes.push(response.rows[i].cellsArray[0])
		dadosAtividadesSociaisAntes.push(Number(response.rows[i].cellsArray[1]))
	}
	graphAtividadesSociaisAntes();
}
function graphAtividadesSociaisAntes(){	
	var ctx = document.getElementById("graphAtividadesSociaisAntes").getContext('2d');
	var graphAtividadesSociaisAntes = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: labelsAtividadesSociaisAntes,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosAtividadesSociaisAntes
			}]
		}
	});
}
$('#atividadesSociaisAtual').sheetrock({
	url: planilha,
	query: "select AQ, AT, AU",
	fetchSize: 4,
	headersOff: true,
	labels: ['Atividades','Quantidade', 'Percentual (%)'],
	callback: retornoAtividadesSociaisAtualFunc
});
function retornoAtividadesSociaisAtualFunc(error, options, response) {
	for (var i = 1; i <= response.rows.length - 1; i++){
		labelsAtividadesSociaisAtual.push(response.rows[i].cellsArray[0])
		dadosAtividadesSociaisAtual.push(Number(response.rows[i].cellsArray[1]))
	}
	graphAtividadesSociaisAtual();
}
function graphAtividadesSociaisAtual(){
	var ctx = document.getElementById("graphAtividadesSociaisAtual").getContext('2d');
	var graphAtividadesSociaisAtual = new Chart(ctx, {
		type: 'doughnut',
		data: {
			labels: labelsAtividadesSociaisAtual,
			datasets: [{
				label: "Participantes",
				backgroundColor: ['#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				data: dadosAtividadesSociaisAtual
			}]
		}
	});
}
$(window).ready(function(){
	var altura = $(window).height();
    altura = ((altura/2) - (altura/10));
    $('.teste').css('margin', altura + 'px 0');
	if ($(window).width() < 560){
		Chart.defaults.global.legend.display = false;
	}
	else {
		Chart.defaults.global.legend.display = true;
	}
});
$(window).resize(function() {
    var altura = $(window).height();
    altura = ((altura/2) - (altura/10));
    $('.teste').css('margin', altura + 'px 0');
	if ($(window).width() < 560 && (Chart.defaults.global.legend.display)) {
		Chart.defaults.global.legend.display = false;
		graphIdade();
		graphCurso();
		graphAnoIngresso();
		graphComoViveAntes();
		graphComoViveAtualmente();
		graphFonteRendaAntes();
		graphFonteRendaAtual();
		graphAtividadesSociaisAntes();
		graphAtividadesSociaisAtual();
	}
	else if ($(window).width() >= 560 && (!Chart.defaults.global.legend.display)) {
		Chart.defaults.global.legend.display = true;
		graphIdade();
		graphCurso();
		graphAnoIngresso();
		graphComoViveAntes();
		graphComoViveAtualmente();
		graphFonteRendaAntes();
		graphFonteRendaAtual();
		graphAtividadesSociaisAntes();
		graphAtividadesSociaisAtual();
	}
});
$(window).scroll(function(){
    var distanceFromTop = $(document).scrollTop();
	if (distanceFromTop > 0 && distanceFromTop < 190){
    
		var altura = $(window).height();
		altura = ((altura/2) - (altura/10)) - (distanceFromTop + (distanceFromTop/2));
		if (altura > 0){
			//$('.teste').css('margin', altura + 'px 0');
			$('.teste').animate({"margin": altura + "px 0"}, 30);
			//console.log(altura);
		}
	}
   if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
		$('.footer').animate({"height": 40 + "%"}, 150);
		$('.fundo').animate({"padding-top": 80 + "px"}, 250);
	}
});
// $('#comoFicouSabendo').sheetrock({
	// url: planilha,
	// query: "select AQ, AT, AU",
	// fetchSize: 4,
	// headersOff: true,
	// labels: ['Atividades','Quantidade', 'Percentual (%)'],
	// callback: retornocomoFicouSabendoFunc
// });
// function retornocomoFicouSabendoFunc(error, options, response) {
	// for (var i = 1; i <= response.rows.length - 1; i++){
		// labelsComoFicouSabendo.push(response.rows[i].cellsArray[0])
		// dadosComoFicouSabendo.push(Number(response.rows[i].cellsArray[1]))
	// }
	// graphComoFicouSabendo();
// }
// function graphComoFicouSabendo(){
	
	// //Chart.defaults.global.legend.display = false;
	
	// var ctx = document.getElementById("graphComoFicouSabendo").getContext('2d');
	// var graphComoFicouSabendo = new Chart(ctx, {
		// type: 'doughnut',
		// data: {
			// labels: labelsAtividadesSociaisAtual,
			// datasets: [{
				// label: "Participantes",
				// backgroundColor: ['#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a'],
				// data: dadosAtividadesSociaisAtual
			// }]
		// }
	// });
// }