$(document).ready(function () {		
	var canGoNext = false;
	$.getJSON('estados_cidades.json', function (data) {
		var items = [];
		var options = '<option value="">escolha um estado</option>';	
		$.each(data, function (key, val) {
			options += '<option value="' + val.sigla + '">' + val.nome + '</option>';
		});
		
		$('#estados1').html(options);
		$('#estados2').html(options);	
			
		$('.estado').change(function () {
			var itemID = ($(this).attr('id'));
		
			var options_cidades = '';
			var str = "";
			
			$('#' + itemID + ' ' + 'option:selected').each(function () {
				str += $(this).text();
			});
			
			$.each(data, function (key, val) {
				if(val.nome == str) {
					$.each(val.cidades, function (key_city, val_city) {
						options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
					});							
				}
			});
			$('#cidades' + itemID[itemID.length-1]).html(options_cidades);	
		});
	});
});

// var formItems = []
// , formItemsUnique = [];

// $(".form__item").each(function() {
// 	formItems.push($(this).attr('name'));
// });

// $('.form__item').change(function(){
// 	$.each(formItems, function(i, el){
// 		if($.inArray(el, formItemsUnique) === -1) formItemsUnique.push(el);
// 	});

// 	$.each(formItemsUnique, function(key, value) {
// 		if($('[name=' + value + ']').attr('val').length) {
// 			console.log('hue');
// 		}
// 	});

// 	console.log(formItemsUnique);
// });

function showFullForm() {
	if(!canGoNext) {
		$('.part2--show').removeClass('part2--show');
		return;
	}
		
	$('.part2').addClass('part2--show');
	document.getElementById('botao-proximo').style.display = 'none';
	$("html, body").animate({ scrollTop: $(".part2--show").offset().top }, 900);
}

$('#botao-proximo').click(function() {
	showFullForm();
});

function checarRA() {
	
	if((document.getElementById('ra').value).length < 6 || isNaN(document.getElementById('ra').value)) {
		$('#botao-proximo').addClass('disabled');
		canGoNext = false;
		showFullForm();
		document.getElementById('botao-proximo').style.display = 'inline';
		document.getElementById('checkRA').innerHTML = '';
		$('#botao-proximo').html('Continuar').removeAttr('href');
		return;
	};

	var planilha = 'https://docs.google.com/spreadsheets/d/17Gw3m11Q6LunJdQTk2SiknpGhi-oQ2eojIyVMhxWmpw/edit#gid=0';
	var numeroRA = document.getElementById('ra').value;
	
	sheetrock({
		url: planilha,
		query: "select A,B WHERE B = " + numeroRA,
		callback: myCallback
	});
	
	function myCallback(error, options, response) {
  	var raList;
		if (response.rows[1]) {
			console.log('R.A. ' + response.rows[1].cellsArray[1] + ' já existe no BD');
			raList = response.rows[1].cellsArray[0];
			document.getElementById('checkRA').style.display = 'inline';
			document.getElementById('ra').style.border = "1px solid red";
			document.getElementById('checkRA').innerHTML = 'O portador desse R.A. já respondeu a pesquisa em: ' + raList;
			$('#botao-proximo').html('Ver resultados').attr('href', 'pesquisa.radiogita.com/resultados');
			
		} else {											
			console.log('R.A. ' + document.getElementById('ra').value + ' não existe no BD');
			$('#botao-proximo').removeClass('disabled');
			canGoNext = true;
		}
	}
}
function gerarMoroEmPG(){
	$.getJSON('estados_cidades.json', function (data) {
		var items = [];
		var options = '<option value="">escolha um estado</option>';	
		$.each(data, function (key, val) {
			options += '<option value="' + val.sigla + '">' + val.nome + '</option>';
		});	
		$('#estados2').html(options);		
		document.getElementById('estados2').value = 'PR';			
		var itemID = 'estados2';
		
		var options_cidades = '';
		var str = "";
		
		$('#' + itemID + ' ' + 'option:selected').each(function () {
			str += $(this).text();
		});
		
		$.each(data, function (key, val) {
			if(val.nome == str) {
				$.each(val.cidades, function (key_city, val_city) {
					options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
				});							
			}
		});
		$('#cidades' + itemID[itemID.length-1]).html(options_cidades);
		document.getElementById('cidades2').value = 'Ponta Grossa';
		gerarLocalMorava();
	});
};
function gerarLocalNascimento() {
	document.getElementById('localNascimento').value = 
	document.getElementById('cidades1').value + ', ' + 
	document.getElementById('estados1').value;
}
function gerarLocalMorava() {
	document.getElementById('ondeMorava').value = 
	document.getElementById('cidades2').value + ', ' + 
	document.getElementById('estados2').value;
	
	$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + document.getElementById('ondeMorava').value, function(json_data) {
		document.getElementById('ondeMoravaLatLong').value = ('{"lat": "' + json_data.results[0].geometry.location.lat + '", "lng": "' + json_data.results[0].geometry.location.lng + '"}');
	});
}
function checagem(ele) {
	idElemento = (ele.id);
	idDiv = '';
	idTexto = '';
	switch (idElemento){
		case "opcaoOutro1":
			idDiv = "outro1";
			idTexto = "textoOpcaoOutro1";
			break;
		case "opcaoOutro2":
			idDiv = "outro2";
			idTexto = "textoOpcaoOutro2";
			break;
		case "opcaoOutro3":
			idDiv = "outro3";
			idTexto = "textoOpcaoOutro3";
			break;
		case "opcaoOutro4":
			idDiv = "outro4";
			idTexto = "textoOpcaoOutro4";
			break;
		case "opcaoOutro5":
			idDiv = "outro5";
			idTexto = "textoOpcaoOutro5";
			break;
		case "atividadeAnterior4":
			idDiv = "outro6";
			idTexto = "textoOpcaoOutroAtividadeAnterior";
			break;
		case "atividadeAtual4":
			idDiv = "outro7";
			idTexto = "textoOpcaoOutroAtividadeAtual";
			break;
	}
	if (document.getElementById(idElemento).checked) {
		document.getElementById(idDiv).style.display = 'inline'
		document.getElementById(idTexto).select();
	}
	else {document.getElementById(idDiv).style.display = 'none'}
}
function gerarTextoOpcaoOutroRadio() {
  	if (document.getElementById(idElemento).checked && document.getElementById(idTexto).value.length) {
		document.getElementById(idElemento).value = document.getElementById(idTexto).value;
    } else {
		document.getElementById(idElemento).value = 'Outro';
    }
}
function listarAtividades() {
	var atividade = 'atividadeAtual';
	var texto = 'textoOpcaoOutroAtividadeAtual';
	for(var j=5; j<=6; j++){
		document.getElementById(atividade).value = '';
		for(var i=1; i<=3; i++) {
			if (document.getElementById(atividade + i).checked) {
				if (document.getElementById(atividade).value != '') {
					document.getElementById(atividade).value = document.getElementById(atividade).value + ', ' + document.getElementById(atividade + i).value; 
				} else {
					document.getElementById(atividade).value = document.getElementById(atividade).value + document.getElementById(atividade + i).value;
				}
			}
		}
		if(document.getElementById(atividade + '4').checked && document.getElementById(texto).value.length) {
			if(document.getElementById(atividade).value.length) {
				document.getElementById(atividade).value += ', ';
			}
			document.getElementById(atividade).value += document.getElementById(texto).value;
		} else {
			if(document.getElementById(atividade + '4').checked) {
				if(document.getElementById(atividade).value.length) {
					document.getElementById(atividade).value += ', ';
				}
				document.getElementById(atividade).value += document.getElementById(atividade + '4').value;
			}
		}
		atividade = 'atividadeAnterior';
		texto = 'textoOpcaoOutroAtividadeAnterior';
	}
}
jQuery(document).ready(function($) {
	var request2
		, request;
	$("#form__completo").submit(function(event) {
		event.preventDefault();

		if(!sessionStorage.getItem('hasPermission'))
			return;
		if (request2) {
			request2.abort();
		}
		var $form = $(this);
		var $inputs = $form.find("input, select, button, textarea");
		var serializedData = $form.serialize();

		$inputs.prop("disabled", true);
		$('#result').text('Enviando informações...');

		request2 = $.ajax({
			url: "https://script.google.com/macros/s/AKfycbzwhiIAkdQsZJF4x7DsXE7SwtETmlbnpmhxkoy_q0LA3kJ6TEhg/exec",
			type: "post",
			data: serializedData
		});
		request2.done(function (response, textStatus, jqXHR){
			$('#result').html('<a href="https://docs.google.com/a/radiogita.com/spreadsheets/d/1N9chKEjuLodMwqjzdHYgsN3vXmLdv8UT0Gu2Eo5Gndw/gviz/tq?tqx=out:html&tq&gid=2" target="_blank">Sucesso - Veja a Planilha Google</a>');
			console.log("Oba, funcionou!");
			
			$inputs.val('');
			$inputs.prop("checked", false);
			
		});
		request2.fail(function (jqXHR, textStatus, errorThrown){
			console.error(
				"Aconteceu um erro: "+
				textStatus, errorThrown
			);
		});
		request2.always(function () {
			$inputs.prop("disabled", false);
			$(location).attr('href', 'http://radiogita.com/pesquisateste/resultados/');
		});
	});
	$(".form__consentimento").submit(function(event) {
		event.preventDefault();
		if (request) {
			request.abort();
		}
		var $form = $(this);
		var $inputs = $form.find('input');
		var serializedData = $form.serialize();

		if(document.getElementById('naoconcordo').checked) {
			sessionStorage.setItem('hasPermission', false);
			request = $.ajax({
				url: "https://script.google.com/macros/s/AKfycbzwhiIAkdQsZJF4x7DsXE7SwtETmlbnpmhxkoy_q0LA3kJ6TEhg/exec",
				type: "post",
				data: serializedData
			});
			request.done(function (response, textStatus, jqXHR){
				$('#result').html('<a href="http://pesquisa.radiogita.com/questionario/resultados" target="_blank">Sucesso - Veja os resultados</a>');
				console.log("Oba, funcionou!");
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				console.error(
					"Aconteceu um erro: "+
					textStatus, errorThrown
				);
			});
			request.always(function () {
				$inputs.prop("disabled", false);
			});
			$inputs.prop("disabled", true);
			$('#loading').text('Resposta enviada. Obrigado!');
		} else {
			$('.termo').addClass('termo--hidden');
			$('.blur').removeClass('blur');
			
			$("html, body").animate({ scrollTop: 0 }, "slow");
			$("html, body").animate({ scrollTop: $("#age").offset().top }, 900);
			
			sessionStorage.setItem('hasPermission', true);
		}
	});
});